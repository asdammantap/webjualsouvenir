  <?php 
  error_reporting(0);
	  session_start();
  include "config/koneksi.php";
  include "config/fungsi_indotgl.php";
  include "config/pagingproduk.php";
  include "config/fungsi_combobox.php";
  include "config/library.php";
  include "config/fungsi_autolink.php";
  include "config/fungsi_rupiah.php";
  include "hapus_orderfiktif.php";
  
  if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
  $user="Pengunjung";
  }
  else
  {
	$user="$_SESSION[namalengkap]";  
  }
  ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gte IE 9]>         <html class="no-js gte-ie9"> <![endif]-->
<!--[if gt IE 99]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
      <title>BEL'S SOUVENIR</title>
	  <link rel="icon" type="image/png" id="favicon"
          href="img/iconbar1.png"/>
      <meta name="description" content="">
  
      <meta name="viewport" content="width=device-width">

      <link rel="stylesheet" href="css/normalize.min.css">
      <link rel="stylesheet" href="css/main.css">		
      <link rel="stylesheet" href="css/media-queries.css">		
      <link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/navbar.css">

      <script src="js/vendor/modernizr-2.6.1.min.js"></script>
      
    
    <link rel="stylesheet" href="css_ticker/style.css">  
    <script type="text/javascript" src="js_ticker/jquery.min.js"></script>
	<script type="text/javascript" src="js_ticker/jquery.totemticker.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#vertical-ticker').totemticker({
				row_height	:	'100px',
				next		:	'#ticker-next',
				previous	:	'#ticker-previous',
				stop		:	'#stop',
				start		:	'#start',
				mousestop	:	true,
			});
		});
	</script>


    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->

		
		
		
      <nav id='menu'>
<input type='checkbox'/>
<label>&#8801;<span>Navigation</span></label>
<ul>
<li><a href='?hal=home' title="Home">Beranda</a></li>
<li><a href='?hal=produk-lists' title="Our Product">Produk Kami</a></li>
<li><a href='?hal=carabeli' title="How To Order">Cara  Pemesanan</a></li>
<li><a href='?hal=contact' title="Contact">Kontak</a></li>
<li style="padding-top:10px;float:right;font-weight:bold;padding-right:10px;text-transform:uppercase">

<SCRIPT language=JavaScript>var d = new Date();
            var h = d.getHours();
            if (h <= 11) { document.write('Selamat Pagi <?php echo $user ?>'); }
            else { if (h <= 15) { document.write('Selamat Siang <?php echo $user ?>'); }
            else { if (h <= 19) { document.write('Selamat Sore <?php echo $user ?>'); }
            else { if (h <= 23) { document.write('Selamat Malam <?php echo $user ?>'); }
            }}}</SCRIPT></li>
</ul>
</nav>
      
      
      <header class="container">      
        <div class="row">
        
           
             <div class="span3 shipping">
			
            </div>
			
            <div class="shopping-cart">
              <span class="icon ir">Cart</span>
               <?php
				$sid = session_id();
				$sql = mysql_query("SELECT SUM(jumlah*harga) as total,SUM(jumlah) as totaljumlah FROM orders_temp, produk 
								WHERE id_session='$sid' AND orders_temp.id_produk=produk.id_produk");
					$r=mysql_fetch_array($sql);
					if ($r['totaljumlah'] != ""){
					$total_rp    = format_rupiah($r[total]);	
				    echo "<span class='text'><a href='?hal=cart'><span class='title'>Shopping Cart</span></a> (<span>$r[totaljumlah]</span> items) - </span>
					  <span class='price'><span>Rp.</span><span>$total_rp</span></span>";
					}
					else
					{
					echo "<span class='text'><a href='?hal=cart'><span class='title'>Shopping Cart</span></a> (<span>0</span> items) - </span>
					  <span class='price'><span>Rp.</span><span>0</span></span>";	
					}
			  ?>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<font style="text-transform:uppercase;"><?php
               if (!empty($_SESSION['namauser']) AND !empty($_SESSION['passuser'])){
				?>
                
                  <a href="logout.php">
                    Logout
                  </a>
               
                <?php
			    }
			    
                if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
				?>  
               
                  <a href="?hal=login">
                    Log in
                  </a>
                
                <?php
				}
				?></font>
            </div>
            
          </div>
          
        </div>  

        
      </header>
      
      <?php
	   include "konten.php";
	  ?>
      
      
      
      <footer>
        <div class="top">
          <div class="container" style="width:100%;">
            <div class="row">
            
              <div class="span9" style="float:left;">
                <div class="row-fluid">
                 <h3 style="color:#ffffff;">visit us : Anyer Residence Blok D/3 Anyer Banten-Indonesia
                </div>
              </div>
              
              <div class="span6 clearfix social-wrapper">
                <ul class="social rr clearfix">
                  <li><span>Follow us</span></li>
                  <li><a href="#" class="ir icon tw">Twitter</a></li>
                  <li><a href="http://www.facebook.com/TeoThemes" class="ir icon fb">Facebook</a></li>
                  <li><a href="#" class="ir icon gp">Google plus</a></li>
                  <li><a href="#" class="ir icon rss">RSS</a></li> 
                </ul>
              </div>
              
            </div>
          </div>
        </div>
        <div class="bottom">
          Copyright &copy;&nbsp;<?php echo date('Y'); ?> - Wong Mantap
        </div>
      </footer>
      
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.2.min.js"><\/script>')</script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
