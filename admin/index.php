<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Ruang Administrator</title>
<link rel="stylesheet" href="css_login/style.default.css" type="text/css" />
<link rel="stylesheet" href="css/style.shinyblue.css" type="text/css" />
<link href="css/login.css" rel="stylesheet" type="text/css" /> 

<script type="text/javascript" src="js_login/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js_login/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="js_login/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="js_login/modernizr.min.js"></script>
<script type="text/javascript" src="js_login/bootstrap.min.js"></script>
<script type="text/javascript" src="js_login/jquery.cookie.js"></script>
<script type="text/javascript" src="js_login/custom.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
</head>

<body>

<div class="span5" style="margin-left:450px;margin-top:150px;">		
<div class="navbar-form">	
		 
		<center><img src="../img/users_1.png" width="100"></center>
		<center><h3>FORM LOGIN</h3></center>
		<form action="cek_login.php" method="post"><table>
				<tr><td>Username: </td><td><input type="text" name="username" size="20" required autofocus></td></tr>
				<tr><td>Password: </td><td><input type="password" name="password" size="20"></td></tr>
			<tr><td></td><td><input type="submit" value="Log In" class="btn btn-inverse"></td></tr>
			</form>

</div>		
</div>

</body>
</html>
