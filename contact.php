<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bel's Souvenir</title>
</head>

<body>
      
      <div role="main" class="container contact">        
        <div class="row">
        
          <div class="span5">
            <h1>Contact with us</h1>
            
            <form action="simpan_hubungi.php" method="post" class="clearfix">
              <label>
                <input class="text-input" type="text" name="nama" placeholder="Masukkan Nama Anda..."/>
              </label>
              <label>
                <input class="text-input" type="text" name="email" placeholder="Masukkan Email Anda..."/>
              </label>
              <label>
                <input class="text-input" type="text" name="subjek" placeholder="Masukkan Subjek Anda..."/>
              </label>
              <textarea class="text-input" name="pesan" placeholder="Tulliskan Pesan Anda..."></textarea>
			  <label>
                <img src='captcha.php'>
              </label>
              <br />
              <label>
               (masukkan 6 kode di atas) <input class="text-input" type="text" name="kode" size="10" maxlength="6"/>
              </label>
              <span class="gradient"><input type="submit" value="Submit"/></span>
            </form>
          </div>
          
          <div class="span6 offset1">
            <h1>Tentang Bel's Souvenir</h1>
            
            <p align="justify">
             Bel's Souvenir adalah sebuah toko souvenir yang terletak di banten yang menyediakan berbagai jenis  souvenir menarik
			 untuk pernikahan, ulang tahun, graduation, khitanan, dan lain-lain.
            </p>
			<h1>Rincian Kontak</h1>
            
            <p>
              Anyer residence Blok D/3 Anyer, Banten, Indonesia
            </p>
            
            <ul class="rr info clearfix">
              <li>
                Phone: <span class="val">081807921121</span>
              </li>
               <li>
                Email: <span class="val">dtitantravel@yahoo.com</span>
              </li>
              <li>
                Pin BB: <span class="val">2A42CE3D</span>
              </li>             
            </ul>
          </div>
        
        </div> 
        
          
        </div>
      </div>    
</body>
</html>